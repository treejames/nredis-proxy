/**
 * 
 */
package com.opensource.netty.redis.proxy.bootstrap;



import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author liubing
 *
 */
public class NRedisProxyServerBootStrap {
	
	public static void main(String[] args) throws Exception {
		new ClassPathXmlApplicationContext(new String[] {"classpath*:redisProxy.xml"}); 
	}
}
